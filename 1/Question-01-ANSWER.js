// QUESTION 1

// Sort the letters in the string s by the order they occur in the string t. You can assume t will not have
// repetitive characters. For s = "weather" and t = "therapyw", the output should be sortByString(s, t) = "theeraw". 
// For s = "good" and t = "odg", the output should be sortByStrings(s, t) = "oodg".

function sortByString(s, t) {
    // Split s and pull each letter as arguments into sort method
    return s.split("").sort((a, b) => {
            // Sort the s string by the index of the letters in the t string
            if (t.indexOf(a) < t.indexOf(b)) {
                return -1
            }
            else if (t.indexOf(a) > t.indexOf(b)) {
                return 1
            }
        else return 0
        }
        // Rejoin the string
    )
    .join("")
}

console.log(sortByString('weather', 'therapyw'));
console.log(sortByString('good', 'odg'));