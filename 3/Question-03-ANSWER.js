// QUESTION 3

// Your quirky boss collects rare, old coins. They found out you're a programmer and asked you to solve something 
// they've been wondering for a long time.  Write a function that, given an amount of money and an array of coin 
// denominations, computes the number of ways to make the amount of money with coins of the available denominations. 
// Example: for amount=4 (4¢) and denominations=[1,2,3] (1¢, 2¢ and 3¢), your program would output 4—the number of 
// ways to make 4¢ with those denominations: 

// 1¢, 1¢, 1¢, 1¢
// 1¢, 1¢, 2¢
// 1¢, 3¢
// 2¢, 2¢

function changePossibilities(amount, coinArr) {

    // Create array for amount of combinations
      let array = [];
  
      for (let i = 1; i <= amount; i++) {
          array[i] = 0;
      }
      // Skip over 0 since it wont be included
      array[0] = 1;
  
      // Run through each coin
      coinArr.forEach(function(coin){
        // Checks if one is availble to use &
        // if coins leftover are able to be used after
          for (let j = coin; j <= amount; j++) {
              let x = j - coin;
              array[j] += array[x];
          }
      });
  
      // Result is amount of possibilities
      return array[amount];
  }
  
  
  
  console.log(changePossibilities(4, [1,2,3]));