// QUESTION 2 

// Given an encoded string, return its corresponding decoded string.

// The encoding rule is: k[encoded_string], where the encoded_string inside the square brackets is repeated exactly k times.
// Note: k is guaranteed to be a positive integer.

// For s = "4[ab]", the output should be decodeString(s) = "abababab"
// For s = "2[b3[a]]", the output should be decodeString(s) = "baaabaaa"

function decodeString(s){

    // Checks for one or more digits and unlimited amount of characters in between square brackets
	let regex = /(\d+)\[([a-z]*)\]/gi;
    // Replaces the match in the string with subtring(the characters between brackets) that is repeated
	let result = s.replace(regex, (match, num, substr) => substr.repeat(num));
    // Checks for a match in the output string
	let tested = regex.test(result);

    // Checks if string conditions are met still
	if(tested){
		return decodeString(result);
    }
    // Outputs answer
	return result;
}

console.log(decodeString("2[b3[a]]"));

